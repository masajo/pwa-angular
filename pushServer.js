// Archivo donde se crea un servidor Node (simulación de Backend para el ejercicio) con framework llamado Express para
// definir unos endpoints a los que la aplicación de Angular
// puede atacar y recibir notificaciones push gracias a Web-Push

/**
 * Para empezar, descatar que este archivo es JS y lo va a ejecutar NODE
 * Dado que no es TS, ni tenemos un TSConfig, aquí vamos a usar JS puro (common js)
 * Usaremos require en vez de import
 */

let express = require('express');
let bodyParser = require('body-parser');
let cors = require('cors');
let webPush = require('web-push');


// * 1. INTANCIAR NUESTRA APLICACIÓN EXPRESS
let app = express();

app.use(bodyParser.urlencoded({
  extended: false
}));

// * 2. VAMOS A DECIR QUE NUESTRA APLICACIÓN CONSUME - JSON
app.use(bodyParser.json());

// * 3. CONFIGURACIÓN DE CORS
// Vamos a poner que la aplicación puede recibir peticiones de cualquier origen
app.use(cors());

// * 4. PLANTEAMIENTO DE ENDPOINTS DE NUESTRA APLICACIÓN EXPRESS

// Endpoint Raíz: GET --> localhost:3000/
app.get('/', (req, res) => {

  res.status(200).json({
    mensaje: "API funcionando correctamente"
  });

});


// Endpoint en el que un cliente de esta aplicación puede suscribirse para recibir
// Notificaciones Push
// Endpoint Subscribe: POST --> localhost:3000/subscribe
app.post('/subscribe', (req, res) => {

  // Obtener el cuerpo de la petición del cliente
  let body = req.body;

  // Vamos a setear las VAPID KEYS de WEB PUSH para poder
  // Conectarnos con el cliente y enviar mensajes
  // Obtenidas con el comando por terminal:
  // > web-push generate-vapid-keys --json
  webPush.setVapidDetails(
    'mailto: martin@imaginagroup.com',
    // CLAVE PÚBLICA
    'BLTRTqQdnPL84g3VV5egvCIeMPQU4IkMRuBpVtOzUzbpC4Sm_xH4fml3H8ckkNcLDZ_EaaglcDblAZktLu_q-yc',
    // CLAVE PRIVADA
    'X383PIN2OKptnXPTqd5pB3XeQDSgdSghniOeLByN3Dk'
  );

  // Creamos el contenido de la notificación que vamos a mandar
  let notificacion = JSON.stringify(
   {
     "notification": {
      "title": "Martin API",
      "body": "Gracias por suscribirte a la newsletter de Martín API.",
      "icon": "https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-bell-512.png"
    }
  })

  // ENVIAR NOTIFICACIÓN usaremos una PROMESA
  Promise.resolve(
    webPush.sendNotification(body, notificacion)
  ).then(() => {
    res.status(200).json(
      {
        mensaje: 'Notificación enviada correctamente'
      }
    );
  }).catch((error) => {
    console.log('Error en el servidor', error);
    res.status(500);
  });

})







// * 5. DESPLIEGUE DE APLICACIÓN EN LOCALHOST:3000
app.listen(3000, () => {
  console.log('Aplicación Express escuchando en el puerto 3000...')
});


