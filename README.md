# AngularPwa

Proyecto de ejemplo para crear PWAs en Angular y Crear un servidor Express con Node JS que envía
notificaciones push a los clientes que se conectan con su clave pública a través de WebPush.

1. `npm install` para instalar todas las dependencias del proyecto
2. `npm run start:server` para iniciar la aplicación de Express (PushServer.js) que envía notificaciones push en http://localhost:3000. Internamente ejecuta NODEMON con el archivo pushServer.js
3. `npm run deploy:prod` para generar la build del proyecto Angular y desplegarlo en http://localhost:8080
