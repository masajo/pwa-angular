import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private _http: HttpClient) { }


  // Método que nos devuelve una frase aleatoria sobre Chuck Norris
  obtenerFraseAleatoria(): Observable<any> {
    // Realizamos una petición HTTP de tipo GET para obtener el JSON de vuelta
    return this._http.get('https://api.chucknorris.io/jokes/random');
  }

  // Método que se conecta con nuestra aplicación Express
  // y que resulta enla recepción de una notificación push
  // desde WebPush
  suscribirse(push: PushSubscription) {

    // Especificamos en endpoint del cual vamos a recibir notificaciones PUSH
    // Haciéndole un POST a la ruta y pasándole la suscripción Push para poder
    // recibir notificaciones.
    // La puedes encontrar en el la aplicación Express (PushServer.js)
    const URL = 'http://localhost:3000/subscribe';
    // Le pasamos a PushServer en el body, la suscripción push
    return this._http.post(URL, push);

  }


}
