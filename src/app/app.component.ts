import { Component, OnInit } from '@angular/core';
import { SwUpdate, SwPush } from '@angular/service-worker';

import { environment } from '../environments/environment.prod';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'angular-pwa';

  // Frase a renderizar
  frase: string = '';

  ngOnInit() {
    // * Nada más iniciarse, solicitamos que se revise si hay una nueva versión
    this.recargarCache();
  }

  constructor(
    private _swUpdate: SwUpdate, // para actualziar la aplicación con el ServiceWorker
    private _swPush: SwPush, // Para gestionar las notificaciones push con el ServiceWorker
    private _dataService: DataService
  ) { }


  // * Método encargado de recargar la aplicación cuando el ServiceWorker detecta
  // que hay una nueva versión de aplicación con cambios que deben cargarse
  recargarCache() {

    // Si estamos escuchando actualizaciones
    // (hay una nueva versión disponible de la aplicación que ha detectado el SW)
    if (this._swUpdate.isEnabled) {
      this._swUpdate.available.subscribe(
        (event) => {
          /**
           * Cada vez que haya un cambio en la aplicación
           * se va a forzar el refresco de la misma.
           *
           * El service worker obtiene una nueva aplicación actualizada.
           *
           * Se le solicita al usuario que acepte la actualización de app
           * para poder ver los nuevos cambios aplicados
           */
          if (confirm('Hay una nueva versión disponible. ¿Desea cargarla?')) {
            // Actualizamos a la última versión
            this._swUpdate.activateUpdate()
              .then(
                () => window.location.reload() // Forzamos la recarga: ctrl + f5 / shift + command + R
              )
              .catch(
                (error) => alert(`Ha habido un error al obtener la frase: ${error}`))
          }
        },
        (error) => alert(`Ha habido algún error al cargar una nueva versión: ${error}`),
        () => console.log('Nueva versión de la app disponible y cargada')
      );
    }
  }

  // Obtener una nueva frase aleatoria sobre Chuck Norris
  obtenerNuevaFrase() {
    this._dataService.obtenerFraseAleatoria().subscribe(
      (respuesta: any) => {
        if (respuesta.value) {
          this.frase = respuesta.value;
        } else {
          this.frase = '';
        }
      },
      (error) => alert(`Ha habido un error al obtener la frase: ${error}`),
      () => console.log('Nueva frase obtenida correctamente')
    )
  }

  // * Método para gestionar una suscripción de mensajes y poder recibirlos
  // desde la aplicación Express creada en PushServer.js
  suscribirseANewsletter() {

    if (this._swUpdate.isEnabled) {
      // Solicitamos la suscripción
      this._swPush.requestSubscription(
        {
          serverPublicKey: environment.publicKey
        }
      ).then((subscripcion: PushSubscription) => {
        // Nos suscribimos a partir del dataService y esperamos a recibir
        // notificaciones push:
        this._dataService.suscribirse(subscripcion).subscribe();
      }).catch((error) => alert(`Ha habido un error al solicitar la suscripción: ${error}`))
    }

  }

}
